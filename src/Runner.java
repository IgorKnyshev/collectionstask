import by.training.LFU;
import by.training.LRU;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Runner {

    private static final Logger log = LogManager.getLogger("Runner");

    public static void main(String[] args) {
        LRU<String> lru = new LRU<>();
        lru.put("1");
        lru.put("2");
        lru.put("3");
        lru.put("4");
        lru.put("5");

        Runnable firstRun = new Runnable(){
            public void run(){
                lru.put("10");
                lru.get(0);
            }
        };

        Runnable secondRun = new Runnable(){
            public void run(){
                lru.put("11");
                lru.get(4);
            }
        };

        Thread firstThread = new Thread(firstRun);
        Thread secondThread = new Thread(secondRun);
        firstThread.start();
        secondThread.start();
        lru.show();
        log.info(lru.getSize());

        log.info("===========");

        LFU<String, String> lfu = new LFU<>();
        lfu.put("a", "1");
        lfu.put("b", "2");
        lfu.put("c", "3");
        lfu.put("d", "3");

        Runnable thirdRun = new Runnable(){
            public void run() {
                lfu.put("e", "5");
                lfu.get("d");
            }
        };

        Runnable fourthRun = new Runnable(){
            public void run() {
                lfu.put("f", "6");
                lfu.get("a");
            }
        };
        Thread thirdThread = new Thread(thirdRun);
        Thread fourthThread = new Thread(fourthRun);
        thirdThread.start();
        fourthThread.start();
        lfu.show();
        log.info(lfu.getSize());
    }

}
