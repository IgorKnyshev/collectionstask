package by.training;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LFU<K, V> {

    private Lock lock = new ReentrantLock();
    private static final Logger log = LogManager.getLogger("LFU");                 //logger
    private double evictionFactor;
    private int cacheSize;
    private HashMap<K, V> data = new HashMap<>();                                        //No obsolete collections
    private List<K> initialList = new LinkedList<>();                                    //
    private List<K> listF1 = new LinkedList<>();                                         //
    private List<K> listF2 = new LinkedList<>();                                         //Collections isn't synchronized
    private List<K> listF3 = new LinkedList<>();                                         //


    public LFU() {
        cacheSize = 4;
        evictionFactor = 0.8;
    }

    public LFU(int cacheSize, double evictionFactor) {
        this.cacheSize = cacheSize;
        if(evictionFactor<0 || evictionFactor>1)
            throw new IllegalArgumentException("Eviction factor is a value between 0 and 1.");
        this.evictionFactor = evictionFactor;
    }

    public void put(K key, V value) {
        lock.lock();
        try {
            if (!data.containsKey(key)) {
                data.put(key, value);
            }
        } finally {
            lock.unlock();
        }
        if (getSize() + 1 > cacheSize) {                                            //Need to evict?
            lock.lock();
            int elementsDelete = (int) (evictionFactor * cacheSize);
            if (initialList.size() < elementsDelete) {                              //Evict elements amount > current list elements amount
                elementsDelete -= initialList.size();
                initialList.clear();
                for (K elem : initialList) {
                    data.remove(elem);
                }
                if (listF1.size() < elementsDelete) {
                    elementsDelete -= listF1.size();
                    listF1.clear();
					for (K elem : listF1) {
						data.remove(elem);
					}
                    if (listF2.size() < elementsDelete) {
                        elementsDelete -= listF2.size();
                        listF2.clear();
						for (K elem : listF2) {
							data.remove(elem);
						}
                        if (listF3.size() < elementsDelete) {
                            listF3.clear();
							for (K elem : listF3) {
								data.remove(elem);
							}
                            listF3.add(key);
                            lock.unlock();
                        } else {
                            while (elementsDelete != 0) {
                                elementsDelete--;
                                data.remove(listF3.get(0));
                                listF3.remove(0);
                            }
                            if (!exist(key)) {
                                listF3.add(key);
                            }
                            lock.unlock();
                        }
                    } else {
                        while (elementsDelete != 0) {                                //Evict elements amount < current list elements amount
                            elementsDelete--;
                            data.remove(listF2.get(0));
                            listF2.remove(0);
                        }
                        if (!exist(key)) {
                            listF2.add(key);
                        }
                        lock.unlock();
                    }
                } else {
                    while (elementsDelete != 0) {
                        elementsDelete--;
                        data.remove(listF1.get(0));
                        listF1.remove(0);
                    }
                    if (!exist(key)) {
                        listF1.add(key);
                    }
                    lock.unlock();
                }
            } else {
                while (elementsDelete != 0) {
                    elementsDelete--;
                    data.remove(initialList.get(0));
                    initialList.remove(0);
                }
                if (!exist(key)) {
                    initialList.add(key);
                }
                lock.unlock();
            }
        }
        else {
            lock.lock();
            initialList.add(key);
            lock.unlock();
        }

    }

    public V get(K key) {
        lock.lock();
        for(int i = 0; i < initialList.size(); i++) {
            if (initialList.get(i).equals(key)) {
                initialList.remove(i);
                listF1.add(key);
                lock.unlock();
                return data.get(key);
            }
        }
        for(int i = 0; i < listF1.size(); i++) {
            if (listF1.get(i).equals(key)) {
                listF1.remove(i);
                listF2.add(key);
                lock.unlock();
                return data.get(key);
            }
        }
        for(int i = 0; i < listF2.size(); i++) {
            if (listF2.get(i).equals(key)) {
                listF2.remove(i);
                listF3.add(key);
                lock.unlock();
                return data.get(key);
            }
        }
        for(int i = 0; i < listF3.size(); i++) {
            if (listF3.get(i).equals(key)) {
                listF3.remove(i);
                listF3.add(key);
                lock.unlock();
                return data.get(key);
            }
        }
        lock.unlock();
        return data.get(key);
    }

    private boolean exist(K element) {
        boolean exist = false;
        lock.lock();
        try {
            if (initialList.contains(element) || listF1.contains(element)
                    || listF2.contains(element) || listF3.contains(element)) {
                exist = true;
            }
        } finally {
            lock.unlock();
        }
        return exist;
    }

    public int getSize() {
        int size = 0;
        lock.lock();
        try {
            size = initialList.size()
                    + listF1.size()
                    + listF2.size()
                    + listF3.size();
        } finally {
            lock.unlock();
        }
        return size;
    }

    public void show() {
        log.info("Data:");
        lock.lock();
        try {
            Iterator it = data.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                log.info(pair.getKey() + " = " + pair.getValue());
                it.remove();
            }
        } finally {
            lock.unlock();
        }
        log.info("initialList:");
        showList(initialList);
        log.info("ListF1:");
        showList(listF1);
        log.info("ListF2:");
        showList(listF2);
        log.info("ListF3:");
        showList(listF3);
    }

    private void showList(List<K> list) {
        lock.lock();
        try {
            for (K elem : list) {
                log.info(elem);
            }
        } finally {
            lock.unlock();
        }
    }

}
