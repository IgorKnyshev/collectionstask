package by.training;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LRU<E> {

    private Lock lock = new ReentrantLock();
    private static final Logger log = LogManager.getLogger("LRU");             //logger
    private final int cacheSize;
    private List<E> list = new LinkedList<>();                                       //Collections isn't synchronized
    
    public LRU() {
        this.cacheSize = 5;
    }
    
    public LRU(final int cacheSize) {
        if (cacheSize <= 0) {
            throw new IllegalArgumentException("Wrong argument");
        }
        this.cacheSize = cacheSize;
    }

    public void put(final E element) {
        int position = -1;
        lock.lock();
        try {
            for (int i = 0; i < list.size(); i++) {                                  //Only unique elements in cache!
                if (element.equals(list.get(i))) {                                   //Does element already exist in cache?
                    position = i;                                                    //Get position
                    break;
                }
            }
        } finally {
            lock.unlock();
        }

        if (position == -1) {                                                        //Is it new element?
            lock.lock();
            try {
                if (list.size() == cacheSize) {                                      //Is cache full?
                    list.remove(0);                                            //Remove the oldest element
                    list.add(element);                                               //Add the latest element
                } else {
                    list.add(element);
                }
            } finally {
                lock.unlock();
            }
        }
    }

    public E get(final int number) {
        E element = null;
        lock.lock();
        try {
            element = list.get(number);
            list.remove(number);                                                       //Get element = this element become last
            list.add(element);                                                         //
        } finally {
            lock.unlock();
        }
        return element;
    }
    
    public String getSize() {
        int size = 0;
        lock.lock();
        try {
            size = list.size();
        } finally {
            lock.unlock();
        }
        return "Total capasity of cache = " + cacheSize                               //Get amount of elements and size of cache
                 + " elements. Current amount = "
                + size + " elements.";
    }
    
    public void show() {
        log.info("List:");
        lock.lock();
        try {
            for (E element : list) {
                log.info(element);
            }
        } finally {
            lock.unlock();
        }
    }
    
}
